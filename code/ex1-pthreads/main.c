#include <stdio.h>
#include <pthread.h>

void tarefa_a(int *);
void tarefa_b(int *);
void tarefa_final(int, int);

int res1 = 0, res2 = 0;

int main(void) {
  pthread_t thread1, thread2;

  pthread_create(&thread1, NULL, (void *) tarefa_a, (void *) &res1);
  pthread_create(&thread2, NULL, (void *) tarefa_b, (void *) &res2);

  pthread_join(thread1, NULL);
  pthread_join(thread2, NULL);

  tarefa_final(res1, res2);
  return 0;
}

void tarefa_a(int *contador) {
  int i, j, x;

  for (i = 0; i < 4; i++) {
    printf("Tarefa A\n");
    for (j = 0; j < 10000; j++) x += i;
    (*contador)++;
  }
}

void tarefa_b(int *contador) {
  int i, j, x;

  for (i = 0; i < 4; i++) {
    printf("Tarefa B\n");
    for (j = 0; j < 10000; j++) x += i;
    (*contador)++;
  }
}

void tarefa_final(int a, int b) {
  int total;
  total = a + b;
  printf("Tarefa A executou %d vezes e B %d vezes. Total: %d\n",
      a, b, total);
}
