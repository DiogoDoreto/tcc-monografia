#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

#define N 100000000

int main(int argc, char **argv) {
  int i;
  double *vector, mean, stddeviation, start_time, duration;

  vector = (double *) malloc(N * sizeof(double));
  for (i = 0; i < N; i++) {
    vector[i] = rand();
  }

  start_time = omp_get_wtime();
  mean = 0;
  stddeviation = 0;
#pragma omp parallel default(shared) private(i)
  {
#pragma omp for reduction(+:mean)
    for (i = 0; i < N; i++) {
      mean += vector[i/10];
    }
#pragma omp single
    mean /= N;

#pragma omp for reduction(+:stddeviation)
    for (i = 0; i < N; i++) {
      stddeviation += (vector[i] - mean)*(vector[i] - mean);
    }
  }
  stddeviation = sqrt(stddeviation / N);
  duration = omp_get_wtime() - start_time;

  printf("Std. Deviation = %lf\n", stddeviation);
  printf("Duration = %lfms\n", duration * 1000);
}
