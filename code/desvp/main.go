package main

import (
	"fmt"
	"math"
	"math/rand"
	"runtime"
	"time"
)

const N = 100000000

func Sum(v []float64) float64 {
	sum := v[0]
	for _, num := range v[1:] {
		sum += num
	}
	return sum
}

// splitWork divide o vetor `v` em `workers` partes, enviando
// cada slice resultante no channel `ch`
func splitWork(v []float64, ch chan []float64, workers int) {
	remWorkers := workers
	remItems := N //len(v)
	var slice []float64

	for remWorkers > 0 {
		size := remItems / remWorkers
		slice, v = v[:size], v[size:]
		remWorkers--
		remItems -= size

		ch <- slice
	}
}

func main() {
	// lê a quantidade de threads que serão usadas por esse programa
	workers := runtime.GOMAXPROCS(0)

	values := make([]float64, N)

	for i := range values {
		values[i] = rand.Float64()
	}

	work := make(chan []float64, workers)
	res := make(chan float64, workers)

	start := time.Now()
	// divide a carga de trabalho
	go splitWork(values, work, workers)

	// soma cada parte
	for i := 0; i < workers; i++ {
		go func(res chan<- float64, work <-chan []float64) {
			res <- Sum(<-work)
		}(res, work)
	}

	// soma o resultado de cada parte
	sum_v := 0.0
	for i := 0; i < workers; i++ {
		sum_v += <-res
	}

	// calcula a média
	N := float64(len(values))
	mean_v := sum_v / N

	// divide novamente a carga de trabalho
	go splitWork(values, work, workers)
	// calcula a diferença quadrática de cada parte
	for i := 0; i < workers; i++ {
		go func(mean_v float64, res chan<- float64, work <-chan []float64) {
			sum := 0.0
			for _, v := range <-work {
				d := v - mean_v
				sum += d * d
			}
			res <- sum
		}(mean_v, res, work)
	}

	// soma cada parte
	desv := 0.0
	for i := 0; i < workers; i++ {
		desv += <-res
	}
	// e aplica a raiz quadrada
	desv = math.Sqrt(desv / N)
	duration := time.Since(start)

	fmt.Println("s =", desv)
	fmt.Println("Duration =", duration.Seconds()*1000)
}
