class Main {
  public static void main(String[] args) {
    TarefaA a = new TarefaA();
    TarefaB b = new TarefaB();

    Thread thread1 = new Thread(a);
    Thread thread2 = new Thread(b);
    thread1.start();
    thread2.start();

    try {
      thread1.join();
      thread2.join();
    } catch (InterruptedException e) {
      System.err.println("Nao foi possivel aguardar as Threads.");
    }
    tarefa_final(a.contador, b.contador);
  }

  static void tarefa_final(int a, int b) {
    int total = a + b;
    System.out.printf("Tarefa A executou %d vezes e B %d vezes. Total: %d\n",
        a, b, total);
  }
}

class TarefaA implements Runnable {
  public int contador = 0;

  @Override
  public void run() {
    int x = 0;
    for (int i = 0; i < 4; i++) {
      System.out.println("Tarefa A");
      for (int j = 0; j < 10000; j++) x += i;
      contador++;
    }
  }
}

class TarefaB implements Runnable {
  public int contador = 0;

  @Override
  public void run() {
    int x = 0;
    for (int i = 0; i < 4; i++) {
      System.out.println("Tarefa B");
      for (int j = 0; j < 10000; j++) x += i;
      contador++;
    }
  }
}
