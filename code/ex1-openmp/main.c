#include <stdio.h>
#include <omp.h>

void tarefa_a(int *);
void tarefa_b(int *);
void tarefa_final(int, int);

int res1 = 0, res2 = 0;

int main(void) {
#pragma omp parallel sections \
  shared(res1, res2)
  {
    tarefa_a(&res1);
#pragma omp section
    tarefa_b(&res2);
  }
  tarefa_final(res1, res2);
  return 0;
}

void tarefa_a(int *contador) {
  int i, j, x;

  for (i = 0; i < 4; i++) {
    printf("Tarefa A\n");
    for (j = 0; j < 10000; j++) x += i;
    (*contador)++;
  }
}

void tarefa_b(int *contador) {
  int i, j, x;

  for (i = 0; i < 4; i++) {
    printf("Tarefa B\n");
    for (j = 0; j < 10000; j++) x += i;
    (*contador)++;
  }
}

void tarefa_final(int a, int b) {
  int total;
  total = a + b;
  printf("Tarefa A executou %d vezes e B %d vezes. Total: %d\n",
      a, b, total);
}
