from multiprocessing import Process, Pipe

def tarefa_a(res):
    contador, x = 0, 0
    for i in range(4):
        print 'Tarefa A'
        for j in xrange(10000):
            x = x + i
        contador += 1
    res.send(contador)

def tarefa_b(res):
    contador, x = 0, 0
    for i in range(4):
        print 'Tarefa B'
        for j in xrange(10000):
            x = x + i
        contador += 1
    res.send(contador)

def tarefa_final(a, b):
    total = a + b
    print 'Tarefa A executou %d vezes e B %d vezes. Total: %d' % (a, b, total)

if __name__ == '__main__':
    parent_conn1, child_conn1 = Pipe()
    parent_conn2, child_conn2 = Pipe()

    p1 = Process(target=tarefa_a, args=(child_conn1,))
    p2 = Process(target=tarefa_b, args=(child_conn2,))
    p1.start(); p2.start()
    tarefa_final(parent_conn1.recv(), parent_conn2.recv())
