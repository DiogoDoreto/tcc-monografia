package main

import "fmt"

func fibonacci() <-chan int {
	ch := make(chan int)
	a, b := 0, 1
	go func() {
		for {
			a, b = b, a+b
			ch <- a
		}
	}()
	return ch
}

func main() {
	fib := fibonacci()

	for i := 0; i < 20; i++ {
		fmt.Printf("%v ", <-fib)
	}
	// Saída:
	// 1 1 2 3 5 8 13 21 34 55
}
