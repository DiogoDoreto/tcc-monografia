package main

import (
	"runtime"
	"sync"
)

const (
	tam   = 1.0
	dx    = 0.00001
	dt    = 0.000001
	T     = 0.01
	kappa = 0.000045
)

func main() {
	// lê a quantidade de threads que serão usadas por esse programa
	workers := runtime.GOMAXPROCS(0)

	// Calculando quantidade de pontos
	n := int(tam / dx)

	// Alocando vetores
	u := make([]float64, n+1)
	u_prev := make([]float64, n+1)

	x := dx
	for i := 1; i < n; i++ {
		if x <= 0.5 {
			u_prev[i] = 200 * x
		} else {
			u_prev[i] = 200 * (1.0 - x)
		}
		x += dx
	}

	t := 0.0
	wg := new(sync.WaitGroup)
	for t < T {
		wg.Add(workers)
		for w := 0; w < workers; w++ {
			go func(w, n int, u, u_prev []float64) {
				// cálculo do índice inicial e final de cada parte
				step := (n - 1) / workers
				end := int((w+1)*step) + 1
				for i := int(w*step) + 1; i < end; i++ {
					u[i] = u_prev[i] + kappa*dt/(dx*dx)*(u_prev[i-1]-2*u_prev[i]+u_prev[i+1])
				}
				wg.Done()
			}(w, n, u, u_prev)
		}
		// barreira: aguarda a finalização do cálculo de todas as goroutines
		wg.Wait()

		// força condição de contorno
		u[0] = 0
		u[n] = 0

		u, u_prev = u_prev, u
		t += dt
	}

	// calculando o maior valor e sua localização
	maxloc := 0
	for i := 1; i < n+1; i++ {
		if u[i] > u[maxloc] {
			maxloc = i
		}
	}
}
