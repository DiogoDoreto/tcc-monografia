#include <stdio.h>
#include <omp.h>

#define u 2.0
#define N 100000000

int main(int argc, char **argv) {
  double passo, soma, x;
  int i;

  passo = (u-1) / N;

#pragma omp parallel for private(i) reduction(+:soma)
  for (i = 0; i < N; i++) {
    x = 1+i*passo;
    soma += 0.5*(1/x+1/(x+passo));
  }
  soma *= passo;
  printf("ln(2) = %lf\n", soma);
  return 0;
}
