package main

import "fmt"

func main() {
	res_a := make(chan int)
	res_b := make(chan int)
	go tarefa_a(res_a)
	go tarefa_b(res_b)
	tarefa_final(<-res_a, <-res_b)
}

func tarefa_a(res chan<- int) {
	var x, contador int

	for i := 0; i < 4; i++ {
		fmt.Println("Tarefa A")
		for j := 0; j < 10000; j++ {
			x += i
		}
		contador++
	}
	res <- contador
}

func tarefa_b(res chan<- int) {
	var x, contador int

	for i := 0; i < 4; i++ {
		fmt.Println("Tarefa B")
		for j := 0; j < 10000; j++ {
			x += i
		}
		contador++
	}
	res <- contador
}

func tarefa_final(a, b int) {
	total := a + b
	fmt.Printf("Tarefa A executou %d vezes e B %d vezes. Total: %d\n", a, b, total)
}
